import { compileNgModuleFromRender2 } from "@angular/compiler/src/render3/r3_module_compiler";
import { Injectable } from "@angular/core";
import { productos, proveedor } from "./products.model";

@Injectable({
  providedIn: "root",
})
export class ProductService {
  private proveedore: proveedor[] = [
    {
      nombre: "Intcomex",
      cedula: 11111,
      direccion: "San Jose",
      numero: 8888,
      correo: "www@deee.com",
      codigo: 333,
    },
    {
      nombre: "Intelec",
      cedula: 11111,
      direccion: "San Jose",
      numero: 88884565,
      correo: "www.intelec.co.cr",
      codigo: 333,
    },
  ];
  private productos: productos[] = [
    {
      proveedor: this.proveedore,
      precio: 888,
      candidad: 12,
      codigo: 1,
      nombre: "Tarjeta Madre Asus Prime X570-P - Socket AM4",
      peso: 1112,
      fecha_caducidad: "16-12-2030",
      descripcion: "Tarjeta madre socket AM4",
    },
    {
      proveedor: this.proveedore,
      precio: 150000,
      candidad: 8,
      codigo: 2,
      nombre: "SSD Adata SC685P- 250GB - USB C 3.2",
      peso: 1113,
      fecha_caducidad: "15-11-2025",
      descripcion: "Interfaz USB 3.2 Gen 2 Tipo C (compatible con las versiones anteriores de USB 2.0)",
    },
  ];
  constructor() {}
  getAll() {
    return [...this.productos];
  }

  getProduct(productId: number) {
    return {
      ...this.productos.find((product) => {
        return product.codigo === productId;
      }),
    };
  }

  deleteProduct(productId: number) {
    this.productos = this.productos.filter((product) => {
      return product.codigo !== productId;
    });
  }

  addProduct(
    pprecio: number,
    pcantidad: number,
    pcodigo: number,
    pnombre: string,
    ppeso: number,
    pfecha_caducidad: string,
    pdescripcion: string
  ) {
    const product: productos = {
      proveedor: this.proveedore,
      precio: pprecio,
      candidad: pcantidad,
      codigo: pcodigo,
      nombre: pnombre,
      peso: ppeso,
      fecha_caducidad: pfecha_caducidad,
      descripcion: pdescripcion,
    };
    this.productos.push(product);
  }

  editProduct(
    pprecio: number,
    pcantidad: number,
    pcodigo: number,
    pnombre: string,
    ppeso: number,
    pfecha_caducidad: string,
    pdescripcion: string
  ) {
    let index = this.productos.map((x) => x.codigo).indexOf(pcodigo);

    this.productos[index].codigo = pcodigo;
    this.productos[index].candidad = pcantidad;
    this.productos[index].nombre = pnombre;
    this.productos[index].peso = ppeso;
    this.productos[index].fecha_caducidad = pfecha_caducidad;
    this.productos[index].descripcion = pdescripcion;
    this.productos[index].precio = pprecio;

    console.log(this.productos);
  }
}
